//requiring and instancing const
const express = require('express');
const app = express();
const http = require('http').Server(app)
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const cParser = require('cookie-parser')
// const updateJsonFile = require('update-json-file')
const fs = require('fs')
const cors = require('cors')
// const Converter = require('hex2dec')
// var SerialPort = require("serialport");
// const Readline = require('@serialport/parser-readline')
const socketIO = require('socket.io')
// var five = require("johnny-five");
// const cron = require('node-cron');
const io = socketIO.listen(http)
//Person Model
const person = require('./src/Models/personModel')
const Mov = require('./src/Controllers/movementController')
const mov = require('./src/Models/movementModel')

//database connect
mongoose.connect('mongodb://127.0.0.1:27017/schedulepas', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true})
.then(db => console.log("Database Connected Successfully"))
.catch(err => console.error("Houston, we have a problem here: \n"+err))

var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

//settings
app.set('PORT', process.env.PORT || 7000);

//Middlewares

app.use(cors({
    origin: 'http://181.57.56.72:3000',
    optionsSuccessStatus: 200
}))
app.use(express.json())
app.use(cParser())
app.use(session({
    key: 'user_session_id',
    secret: '10n1c0s3cr3tc0d3',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: (60*60*1000)
    }
}))

app.get('/', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.sendFile(path.join(__dirname, '/public/index.html'));
    } else {
        res.redirect('/person/login');
    }
})

app.get('/simulate', (req, res) => {
    io.emit('newCode', {code: "561234568"})
    res.send("Está Hecho!")
})

app.get('/logout', (req, res) => {
    req.session.user = null
    res.redirect('/person/login');
})

io.on("connection", socket => {
    console.log("Socket Connection Do it!")
    socket.on("success", function(data) {
        board.write("1")
        console.log('llego')
        setTimeout(() => {
            board.write("4")
        }, 7000);
    })
    socket.on("failed", function(data) {
        board.write("4")
    })        
})

//Routes

app.use('/person', require('./src/Routes/person'));
app.use('/movement', require('./src/Routes/movements'));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/public/index.html'))
})
app.post('/person/movement', (req, res) =>
{    
    var imagedata = req.body.img
    // console.log(req.body.code)
    var base64Data = imagedata.replace(/^data:image\/png;base64,/, "");
    fs.writeFile("public/images/person/"+req.body.code+".png", base64Data, 'base64', async function(err) {
        if(err)
            return res.send('no se guardo')
        let mypers = await person.findOne({code: req.body.code})
        let now = new Date
        let d = now.getDate()
        let m = now.getMonth() + 1
        let y = now.getFullYear()
        let today = m+"/"+d+"/"+y
        today = new Date(today)
        // console.log(mypers.name.first)
        // console.log(today)
        let prevVali = await mov.find({theDay: today, person: mypers._id}).sort({created_at: -1}).limit(1)
        // console.log(prevVali.length)
        if(prevVali.length >= 1 && ('_id' in prevVali[0]) && prevVali[0].outTime == null) {
            // console.log("hay")
            let nowi = new Date
            nowi.setHours(nowi.getHours() - 5)
            let data = await Mov.saveOut(nowi, mypers._id)
            if(data == "nope")
                return res.send('no se guardo')
            else {
                io.emit('outTime', {code: req.body.code})
                return res.send('si se guardo')
            }
        }
        else {
            let dataToSend = {
                person: mypers._id,
                picUri: "images/person/"+req.body.code+".png"
            }
            let data = await Mov.store(dataToSend);
            if(!('_id' in data)) {
                res.json({status: 'failed', data})
            }
            else {
                io.emit('inTime', {code: req.body.code})
                return res.send('si se guardo')
            }
        }
    }); 
})

//Public path
app.use(express.static(path.join(__dirname, '/public')));

//Server listen callback
http.listen(app.get('PORT'), () => {
    console.log(`Server running on port: ${app.get('PORT')}`)
})