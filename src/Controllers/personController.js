const person = require ('../Models/personModel')

class table 
{
    store(request)
    {
        let Data = new person(request)
        //console.log(request)
        var error = Data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;            
        } else{
            let status = Data.save();            
            return status;            
        }
    }

    async upCode(id, code) {
        let validation = await person.findOne({code: code});
        if(validation){
            return {result: 'Codigo duplicado, intente con uno diferente', status: 'failed'}
        }
        else {
            let data = await person.findOne({_id: id});
            if(data) {
                data.code = code
                let result = await data.save()
                return {result, status: 'success'}
            }
            else {
                return {result :'Usuario no encontrado, intentelo mas tarde', status: 'failed'}
            }
        }
    }
}
let Person = new table();
module.exports = Person;