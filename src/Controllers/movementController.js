const mov = require ('../Models/movementModel')
const person = require('../Models/personModel')

class table 
{
    store(request)
    {
        let now = new Date
        now.setHours(now.getHours() - 5)

        let today = new Date
        today.setHours(0)
        today.setMinutes(0)
        today.setSeconds(0)
        today.setMilliseconds(0)
        
        request.theDay = today
        request.inTime = now
        let Data = new mov(request)
        //console.log(request)
        var error = Data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;            
        } else{
            let status = Data.save();            
            return status;            
        }
    }

    async saveOut(time, person) {
        let now = new Date
        let d = now.getDate()
        let m = now.getMonth() + 1
        let y = now.getFullYear()
        let today = m+"/"+d+"/"+y
        today = new Date(today)
        let data = await mov.findOne({theDay: today, person: person, outTime: null})
        if(('_id' in data)) {
            data.outTime = time
            let resi = await data.save()
            if(!('_id' in resi)) {
                return "nope"
            }
            else {
                return "success"
            }
        }
    }

    // async CalculateBalance(range) {
    //     let data = await mov.find({
    //         "created_at": {
    //             "$gte": new Date(range.start), 
    //             "$lte": new Date(range.end)
    //         }
    //     }, {_id: 0, amount: 1, movType: 1, created_at: 1})
    //     let ins = []
    //     let outs = []
    //     let totalin = 0
    //     let totalout = 0
    //     let balance = 0
    //     for (let index = 0; index < data.length; index++) {
    //         if(data[index].movType == 'In') {
    //             let bone = {
    //                 amount: parseInt(data[index].amount),
    //                 date: data[index].created_at
    //             }
    //             ins.push(bone)
    //             totalin += parseInt(data[index].amount)
    //             balance += parseInt(data[index].amount)
    //         }
    //         else {
    //             if(data[index].movType == 'Out') {
    //                 let bone = {
    //                     amount: parseInt(data[index].amount),
    //                     date: data[index].created_at
    //                 }
    //                 outs.push(bone)
    //                 totalout += parseInt(data[index].amount)
    //                 balance -= parseInt(data[index].amount)
    //             }
    //         }
    //     }
    //     return {ins, outs, totalin, totalout, balance}
    // }

    async getActualPersonsStatus() {
        let data = []
        let persondata = await person.find({status: true})
        for (let index = 0; index < persondata.length; index++) {
            let bone = {
                name: persondata[index].name,
                code: persondata[index].code,
                rol: persondata[index].companyRol,
                phone: persondata[index].phone,
                status: false,
                pic: "images/person/"+persondata[index].code+".png",
                in: null,
                out: null
            }
            let lastmov = await mov.find({person: persondata[index]._id}).sort({created_at: -1}).limit(1)
            if(lastmov.length > 0 &&('_id' in lastmov[0])) {
                if(lastmov[0].outTime == null)
                    bone.status = true
                let ini = new Date(lastmov[0].inTime)
                // ini.setHours(ini.getHours() + 5)
                if(lastmov[0].outTime != null) {
                    let outo = new Date(lastmov[0].outTime)
                    // outo.setHours(outo.getHours() + 5)
                    bone.out = (outo.getHours() < 10 ? '0'+outo.getHours() : outo.getHours()) + ':' + (outo.getMinutes() < 10 ? '0'+outo.getMinutes() : outo.getMinutes())
                }
                bone.in = (ini.getHours() < 10 ? '0'+ini.getHours() : ini.getHours()) + ':' + (ini.getMinutes() < 10 ? '0'+ini.getMinutes() : ini.getMinutes())
                
            }
            else
                bone.pic = "images/photo_.png"
            data.push(bone)
        }
        return data
    }

    async getLastMovements(person = null, cant = 10) {
        let data;
        if(person)
            data = await mov.find({person: person}, {_id: 0, theDay: 1, inTime: 1, outTime: 1}).limit(cant);
        else
            data = await mov.find({}, {_id: 0, theDay: 1, inTime: 1, outTime: 1}).limit(cant);
        return data
    }

    async getLateInsByPerson(person) {
        let allData = await mov.find({person: person});
        let data = []
        for (let index = 0; index < allData.length; index++) {
            let that = new Date(allData[index].inTime)
            that.setHours(8)
            that.setMinutes(30)
            that.setSeconds(0)
            if(allData[index].inTime > that)
                data.push(allData[index])
        }
        return data
    }

    async getLateInsGeneralProm(start, end) {
        let allPersons = await person.find({status: true});
        let data = []
        //sconsole.log(allPersons.length)
        for (let ind = 0; ind < allPersons.length; ind++) {
            let allData = await mov.find({person: allPersons[ind]._id, theDay: {
                $gte: start,
                $lte: end
            }}).sort({created_at: 1})
            let count = 0
            let laferef = null
            for (let index = 0; index < allData.length; index++) {
                let that = new Date(allData[index].theDay)
                that.setHours(3)
                that.setMinutes(30)
                that.setSeconds(0)
                if(allData[index].inTime > that && allData[index].theDay != laferef)
                    count++;
                laferef = allData[index].theDay
            }
            let element = {
                person: allPersons[ind]._id,
                lateInsCount: count
            }
            data.push(element)
        }
        return data
    }

    async getToTimeInsGeneralProm(start, end) {
        let allPersons = await person.find({status: true});
        let data = []
        for (let ind = 0; ind < allPersons.length; ind++) {
            let allData = await mov.find({person: allPersons[ind]._id, theDay: {
                $gte: start,
                $lte: end
            }})
            let count = 0
            for (let index = 0; index < allData.length; index++) {
                let that = new Date(allData[index].inTime)
                that.setHours(3)
                that.setMinutes(30)
                that.setSeconds(0)
                if(allData[index].inTime <= that)
                    count++;
            }
            let element = {
                person: allPersons[ind]._id,
                toTimeInsCount: count
            }
            data.push(element)
        }
        return data
    }

    async getJobTimeProm(start, end, personId = null) {
        if(personId) {
            let prom = 0
            let count = 0
            let allData = await mov.find({person: personId, theDay: {
                $gte: start,
                $lte: end
            }, outTime: {$ne: null}})
            for (let index = 0; index < allData.length; index++) {
                count++;
                let promi = new Date(allData[index].outTime) - new Date(allData[index].inTime)
                prom += (promi/(1000*60*60))
            }
            let elpromi = prom/count
            return {person, prom: elpromi}
        }
        else {
            let allPersons = await person.find({status: true});
            let data = []
            for (let ind = 0; ind < allPersons.length; ind++) {
                let allData = await mov.find({person: allPersons[ind]._id, theDay: {
                    $gte: start,
                    $lte: end
                }, outTime: {$ne: null}})
                let count = 0
                let prom = 0
                for (let index = 0; index < allData.length; index++) {
                    count++;
                    let promi = new Date(allData[index].outTime) - new Date(allData[index].inTime)
                    prom += (promi/(1000*60*60))
                }
                let elpromi = prom/count
                let element = {
                    person: allPersons[ind]._id,
                    prom: elpromi
                }
                data.push(element)
            }
            return data
        }
    }

    async getGeneralJobHoursProm(start, end) {
        let allData = await mov.find({
            theDay: {
                $gte: start,
                $lte: end
            },
            outTime: {
                $ne: null
            }
        })

        let datatemp = []
        
        for (let index = 0; index < allData.length; index++) {
            let exist = false
            for (let ind = 0; ind < datatemp.length; ind++) {
                if((Math.abs(new Date(datatemp[ind].day) - new Date(allData[index].theDay))/(1000*60*60*24)) < 1){
                    exist = true
                    datatemp[ind].promi += ((new Date(allData[index].outTime) - new Date(allData[index].inTime))/(1000*60*60))
                    datatemp[ind].count += 1
                }
            }
            if(!exist) {
                let element = {
                    day: allData[index].theDay,
                    promi: ((new Date(allData[index].outTime) - new Date(allData[index].inTime))/(1000*60*60)),
                    count: 1
                }
                datatemp.push(element)
            }
        }
        let data = []
        for (let indd = 0; indd < datatemp.length; indd++) {
            let elpromi = (datatemp[indd].promi / datatemp[indd].count)
            let elel = {
                day: datatemp[indd].day,
                prom: elpromi
            }
            data.push(elel)
        }
        return data
    }
}
let Mov = new table();
module.exports = Mov;