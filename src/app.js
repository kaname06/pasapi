window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// window.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

window.$ = window.jQuery = require('jquery');

$('[data-toggle="tooltip"]').tooltip()

window.Vue = require('vue');

import VueRouter from 'vue-router';
let swal = require('./Utilities/sweetAlert')
// let IO = require('./Utilities/socketIO')

// import io from 'socket.io-client'
// import VueSocketIO from 'vue-socket.io'

// Vue.use(new VueSocketIO({     
//     connection: io('http://localhost:3000') 
// }))

Vue.use(VueRouter);

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
//language
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
const DataTables = require('vue-data-tables')

locale.use(lang)
Vue.use(DataTables)

// import main from './Components/MainComponent.vue'
// import login from './Components/LoginComponent.vue';
// import products from './Components/ProductListComponent.vue';
// import addproducts from './Components/AddProductComponent.vue';


// let paths = {
//     main,
//     login,
//     products,
//     addproducts,

// }

// let ec = [
//     { path: '/lobby', component: paths.main}
//     // { path: '/products', component: paths.products},
//     // { path: '/addproducts', component: paths.addproducts}
// ]
// let cellar = [
//     { path: '/products', component: paths.products},
//     { path: '/addproducts', component: paths.addproducts}
// ]
  
// const ecomm = new VueRouter({
//     uid: 'main',
//     // mode: 'history',   
//     routes: ec // short for `routes: routes`
// })
// const bod = new VueRouter({
//     uid: 'manager',
//     routes: cellar
// })

Vue.component('login-component', require('./Components/MainComponent.vue').default);
Vue.component('graph-component', require('./Components/GraphComponent.vue').default);
Vue.component('wel-component', require('./Components/WelcomeComponent.vue').default);

const main = new Vue({
    el: '#main'
    // router: bod
})




