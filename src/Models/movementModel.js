const { Schema, model} = require('mongoose');

let Movement = new Schema({
    person: {
        type: Schema.Types.ObjectId,
        required: true
    },
    theDay: {
        type: Date,
        required: true
    },
    inTime: {
        type: Date,
        required: true
    },
    outTime: {
        type: Date,
        default: null
    },
    picUri: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
})

module.exports = model('Movement', Movement)