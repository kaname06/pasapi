const { Schema, model} = require('mongoose')

let Person = new Schema({
    name: {
        first: {
            type: String,
            required: true
        },
        last: {
            type: String,
            required: true
        }
    },
    code: {
        type: String,
        default: null
    },
    mail: {
        type: String,
        default: null
    },
    phone: {
        type: String,
        default: null
    },
    status: {
        type: Boolean,
        default: function() {
            if(this.code == null)
                return false
            else
                return true
        }
    },
    companyRol: {
        type: String,
        required: true
    }
})

module.exports = model('Person', Person)