const { Router } = require('express')
const router = Router();
//Person Files
const Person = require('../Controllers/personController')
const person = require('../Models/personModel')
//Mov Files
const Mov = require('../Controllers/movementController')
const mov = require('../Models/movementModel')
var fs = require('fs');
const path = require('path')

let BossAccess = {
    user: "C.OSORIO",
    pass: "Cosorio83",
    lstAccss: null
}

router.post('/image', (req, res) =>
{    
    var imagedata = req.body.img
    console.log(req.body.code)
    var base64Data = imagedata.replace(/^data:image\/png;base64,/, "");
    fs.writeFile("public/images/person/"+req.body.code+".png", base64Data, 'base64', async function(err) {
        if(err)
            return res.send('no se guardo')
        let mypers = await person.findOne({code: req.body.code})
        let now = new Date
        let d = now.getDate()
        let m = now.getMonth() + 1
        let y = now.getFullYear()
        let today = m+"/"+d+"/"+y
        today = new Date(today)
        console.log(mypers.name.first)
        console.log(today)
        let prevVali = await mov.find({theDay: today, person: mypers._id}).sort({created_at: -1}).limit(1)
        console.log(prevVali.length)
        if(prevVali.length >= 1 && ('_id' in prevVali[0]) && prevVali[0].outTime == null) {
            console.log("hay")
            let nowi = new Date
            nowi.setHours(nowi.getHours() - 5)
            let data = await Mov.saveOut(nowi, mypers._id)
            if(data == "nope")
                return res.send('no se guardo')
            else
                return res.send('si se guardo')
        }
        else {
            let dataToSend = {
                person: mypers._id,
                picUri: "images/person/"+req.body.code+".png"
            }
            let data = await Mov.store(dataToSend);
            if(!('_id' in data)) {
                res.json({status: 'failed', data})
            }
            else {
                return res.send('si se guardo')
            }
        }
    }); 
})

router.post('/store', async (req, res) => {
    let data = await Person.store(req.body);
    if(!('_id' in data)) {
        res.json({status: 'failed', data})
    }
    else {
        res.json({status: 'success', data})
    }
})

router.get('/graph', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.sendFile(path.join(__dirname, '../../public/graph.html'));
    } else {
        res.redirect('/person/login');
    }
})

router.post('/getCodes', async (req, res) => {
    let data = await person.find({status: true}, {code: 1, _id: 1})
    let lada = []
    for (let index = 0; index < data.length; index++) {
        let lastmovi = await mov.find({person: data[index]._id}).sort({created_at: -1}).limit(1)
        lada.push({code: data[index].code, lastmov: lastmovi})
    }
    res.json({status: 'success', data: lada})
})

router.post('/getPersons', async (req, res) => {
    let data = await person.find({status: true});
    res.json({status: 'success', data})
})

router.post('/getActualStatus', async (req, res) => {
    let data = await Mov.getActualPersonsStatus()
    if(data.length > 0)
        res.json({status: 'success', data})
    else
        res.json({status: 'failed', data: 'ya valió verga, no hay datos'})
})

router.post('/upCode', async (req, res) => {
    let data = await Person.upCode(req.body.id, req.body.code)
    res.json(data)
})

router.get('/login', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.redirect('/');
    } else {
        res.sendFile(path.join(__dirname, '../../public/login.html'));
    }
})

router.post('/sessdata', (req,res) => {
    if (req.session.user && req.cookies.user_session_id) {
        let temp = req.session.user;
        let data = [];
        for (const key in req.body.fields) {
            let a = req.body.fields[key];
            let ths = temp[a];
            data.push(ths)
        }
        let resp = myCryptor.encode(data)
        res.json({status: 'success', data: resp});
    }
    else {
        res.json({status:'failed'})
    }
})

router.post('/validate', (req, res) => {
    // let data_ = myCryptor.decode(req.body.data);
    let data_ = req.body
    let dat = data_;
    let nick = dat.nickname
    let pass = dat.password
    let resp = ''
        let data = nick === BossAccess.user
        if(data) {
            if(data && (pass === BossAccess.pass)){
                resp = 'Authenticated'
                
                let datitoss = {
                    name: "Carlos Osorio",
                    lastAccess: BossAccess.lstAccss
                }                    
                req.session.user = datitoss
                let lafec = new Date()
                lafec.setHours(lafec.getHours() - 5)
                BossAccess.lstAccss = lafec;
            }
            else
                resp = 'Contraseña incorrecta'
        }
        else
            resp = 'Usuario no encontrado'

    var status = 'success'
    if(resp != 'Authenticated')
        status = 'failed'
    res.json({user: nick, response: resp, status: status, sess: req.session.user})
})


module.exports = router