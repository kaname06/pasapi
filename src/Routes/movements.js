const { Router } = require('express')
const router = Router();
//Movs files
const Mov = require('../Controllers/movementController')
const mov = require('../Models/movementModel')
//Person Files
const person = require('../Models/personModel');
const Person = require('../Controllers/personController')

// router.post('/store', async (req, res) => {
//     let now = new Date
//     let d = now.getDate()
//     let m = now.getMonth() + 1
//     let y = now.getFullYear()
//     let today = m+"/"+d+"/"+y
//     today = new Date(today)
//     let prevVali = await mov.find({theDay: today, person: req.body.person})
//     if(('_id' in prevVali)) {
//         let data = await Mov.saveOut(now, req.body.person)
//         if(data == "nope")
//             res.json({status: 'failed'})
//         else
//             res.json({status: 'success'})
//     }
//     else {
//         let dataToSend = {
//             person: req.body.person,
//             picUri: req.body.picUri
//         }
//         let data = await Mov.store(dataToSend);
//         if(!('_id' in data)) {
//             res.json({status: 'failed', data})
//         }
//         else {
//             res.json({status: 'success', data})
//         }
//     }
    
// })

router.post('/getMovements', async (req, res) => {
    let data = await Mov.getLastMovements(req.body.person, req.body.cant);
    res.json({status: 'success', data})
})

router.post('/getLateInsByPerson', async(req,res)=>{
    let data = await Mov.getLateInsByPerson(req.body.person)
    if(data.length > 0){
        res.json({status: 'success', data})
    }else{
        res.json({status: 'failed', data})
    }
})

router.post('/getLateInsGeneralProm', async(req,res)=>{
    let now = new Date
    let start = new Date();
    start.setDate(start.getDate() - 10)
    let end = now;
    if(req.body.start)
        start = req.body.start
    
    if(req.body.end) 
        end = req.body.end
    
    let data = await Mov.getLateInsGeneralProm(start, end)
    if(data.length > 0){
        res.json({status: 'success', data})
    }else{
        res.json({status: 'failed', data})
    }
})

router.post('/getToTimeInsGeneralProm', async(req,res)=>{
    let now = new Date
    let start = new Date();
    start.setDate(start.getDate() - 10)
    let end = now;
    if(req.body.start)
        start = req.body.start
    
    if(req.body.end) 
        end = req.body.end
    
    let data = await Mov.getToTimeInsGeneralProm(start, end)
    if(data.length > 0){
        res.json({status: 'success', data})
    }else{
        res.json({status: 'failed', data})
    }
})

router.post('/getJobTimeProm', async(req,res)=>{
    let now = new Date
    let start = new Date();
    start.setDate(start.getDate() - 10)
    let end = now;
    if(req.body.start)
        start = req.body.start
    
    if(req.body.end) 
        end = req.body.end
    
    let data = await Mov.getJobTimeProm(start, end, req.body.person)
    if(data.length > 0){
        res.json({status: 'success', data})
    }else{
        res.json({status: 'failed', data})
    }
})

router.post('/getGeneralJobHoursProm', async (req,res)=>{
    let now = new Date
    let start = new Date();
    start.setDate(start.getDate() - 10)
    let end = now;
    if(req.body.start)
        start = req.body.start
    
    if(req.body.end) 
        end = req.body.end
    
    data = await Mov.getGeneralJobHoursProm(start, end)
    if(data.length > 0){
        res.json({status: 'success', data})
    }else{
        res.json({status: 'failed', data})
    }
})
module.exports = router