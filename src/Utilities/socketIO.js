const socketIO = require('../Utilities/ModelsPlugin/socket')

const socketPlugin = {}

socketPlugin.install = function (Vue) {
    Vue.prototype.$IO = socketIO
}

Vue.use(socketPlugin)